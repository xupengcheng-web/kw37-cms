import {NEWS} from '@/utails/http.js';
export const newslist = (curr,limit) => {
    return NEWS({
        method:'get',
        url: "/list",
        params:{curr,limit}
    })
}
export const newstotal = () => {
    return NEWS({
        method:'get',
        url: "/total",
    })
}
export const newsadd = () => {
    return NEWS({
        method:'post',
        url: "/add",
    })
}