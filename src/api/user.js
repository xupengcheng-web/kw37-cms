import request from '@/utails/http.js';
export const login = (data) => {
    return request({
        method:'post',
        url: '/login',
        data
    })
}
export const register = (data) => {
    return request({
        method:'post',
        url: '/register',
        data
    })
}