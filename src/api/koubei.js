import request from '@/utails/http.js';
export const kblist = (params) => {
    return request({
        method:'get',
        url: '/kblist',
        params
    })
}
export const kbadd = (data) => {
    return request({
        method:'post',
        url: '/kbadd',
        data
    })
}
export const kbdel = (data) => {
    return request({
        method:'post',
        url: '/kbdel',
        data
    })
}
export const kbone = (data) => {
    return request({
        method:'post',
        url: '/kbone',
        data
    })
}
export const kbedit = (data) => {
    return request({
        method:'post',
        url: '/kbedit',
        data
    })
}
export const getkb = () => {
    return request({
        method:'get',
        url: '/getkb',
    })
}
