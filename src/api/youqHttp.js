import {YQ} from '@/utails/http.js';

export const yqlist = (curr,limit) => {
    return YQ({
        method:'get',
        url: "/list",
        params:{ curr, limit }
    })
}
export const yqtotal = () => {
    return YQ({
        method:'get',
        url: "/total",
    })
}
export const yqadd = (nameValue,addrValue) => {
    return YQ({
        method:'post',
        url: "/add",
        data:{ sitename: nameValue, siteurl: addrValue }
    })
}
export const yqdel = (delid) => {
    return YQ({
        method:'post',
        url: "/delete",
        data:	{ id: delid  }
    })
}
export const yqedit = (editID,nameValue,addrValue) => {
    return YQ({
        method:'post',
        url: "/edit",
        data:	{ id: editID, sitename: nameValue, siteurl: addrValue }
    })
}