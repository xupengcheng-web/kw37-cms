import Vue from 'vue'
import VueRouter from 'vue-router'
import request from "@/utails/http.js";


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect:"/home"
  },
  {
    path: '/home',
    name: 'Home',
    component: () => import('../views/Home.vue'),
    children:[
      {path:'',component: () => import('../views/Index.vue')},
      {path:'kblist',component: () => import('../views/koubei/Kblist.vue')},
      {path:'kbadd',component: () => import('../views/koubei/Kbadd.vue')},
      {path:'kbedit',component: () => import('../views/koubei/Kbedit.vue')},
      {path:'yqlist',component: () => import('../views/youlian/YouqList.vue')},
      {path:'yqadd',component: () => import('../views/youlian/YouqAdd.vue')},
      {path:'yqedit',component: () => import('../views/youlian/YouqEdit.vue')},
      {path:'newslist',component: () => import('../views/news/NewsList.vue')},
      {path:'newsadd',component: () => import('../views/news/NewsAdd.vue')},
    ]
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login.vue')
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import('../views/Register.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  // console.log(to, from, next);
  if (to.path === "/login" || to.path === "/register") {  
    next();
  } else {

    request({
      method:'post',
      url: "/verifyToken",
    }).then(res => {


      if (!(res.data.code === 0)) {
        localStorage.removeItem('token')
        router.push({path:"/login"})
      } else {
        next();
      }


    })
    
  }
})
export default router
