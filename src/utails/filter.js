import Vue from 'vue';

// 过滤时间
Vue.filter("filterTime",(value) => {
    let t = new Date(value)
    let y = t.getFullYear();
    let m = t.getMonth() + 1;
    let d = t.getDate();
    return `${y}-${addZero(m)}-${addZero(d)}`
})
function addZero(val) {
    return val < 10 ? '0' + val : val
}

// 过滤来源
Vue.filter("filterOrigin",(value) => {
    switch (value) {
        case '1':return"毕业生";
        case '2':return"同学";
        case '3':return"同事";
        case '4':return"网络";
    }
})
