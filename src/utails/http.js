import axios from 'axios';

// kb - axios
const request = axios.create({
    baseURL: 'http://127.0.0.1:3000',
    timeout: 2000,
})

// YQ - axios
const YQ = axios.create({
  baseURL: 'http://8.136.147.250:3000/flink',
  timeout: 2000,
})

// NEWS - axios
const NEWS = axios.create({
  baseURL: 'http://8.136.147.250:3000/article',
  timeout: 2000,
})

// 添加请求拦截器
request.interceptors.request.use(function (config) {
    // 在发送请求之前做些什么
    // 获取 token 并发送给后端
    let token = localStorage.getItem('token')
    // console.log(token);
    if (token) {
        config.headers.token = token; // 设置了一个请求头发送给后端
    }
    return config;
  }, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
  });




export {YQ}
export {NEWS}
export default request

// 创建一个实例
// 你可以创建一个拥有通用配置的axios实例

// axios.creat([config])